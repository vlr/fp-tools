import { expect } from "chai";
import { lPartial } from "../../src";

const source2 = (i1: number, i2: number) => i1 + i2 * 2;

describe("lPartial2", function (): void {
  it("should return partial1 function that sums 2 numbers in correct order", function (): void {
    // arrange

    // act
    const result = lPartial(source2, 2)(1);

    // assert
    expect(result).to.be.equal(4);
  });
});


