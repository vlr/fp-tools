import { expect } from "chai";
import { rPartial4 } from "../../src";

const source4 = (i1: number, i2: number, i3: number, i4: number) => i1 + i2 * 2 + i3 * 3 + i4 * 4;

describe("rPartialNum4", function (): void {
  it("should return partial0 function that sums 4 numbers in correct order", function (): void {
    // arrange

    // act
    const result = rPartial4(source4, 1, 2, 3, 4)();

    // assert
    expect(result).to.be.equal(30);
  });

  it("should return partial1 function that sums 4 numbers in correct order", function (): void {
    // arrange

    // act
    const result = rPartial4(source4, 2, 3, 4)(1);

    // assert
    expect(result).to.be.equal(30);
  });

  it("should return partial2 function that sums 4 numbers in correct order", function (): void {
    // arrange

    // act
    const result = rPartial4(source4, 3, 4)(1, 2);

    // assert
    expect(result).to.be.equal(30);
  });

  it("should return partial3 function that sums 4 numbers in correct order", function (): void {
    // arrange

    // act
    const result = rPartial4(source4, 4)(1, 2, 3);
    // partial4(source4, 4)(1, 2, 3);


    // assert
    expect(result).to.be.equal(30);
  });
});
