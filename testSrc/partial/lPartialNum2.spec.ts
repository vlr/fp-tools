import { expect } from "chai";
import { lPartial1, lPartial2 } from "../../src";

const source1 = i => i;
const source2 = (i1: number, i2: number) => i1 + i2 * 2;

describe("lPartialNum1", function (): void {
  it("should return partial0 function that returns a number", function (): void {
    // arrange

    // act
    const result = lPartial1(source1, 1)();

    // assert
    expect(result).to.be.equal(1);
  });
});

describe("rPartialNum2", function (): void {
  it("should return partial0 function that sums 2 numbers in correct order", function (): void {
    // arrange

    // act
    const result = lPartial2(source2, 1, 2)();

    // assert
    expect(result).to.be.equal(5);
  });

  it("should return partial1 function that sums 2 numbers in correct order", function (): void {
    // arrange

    // act
    const result = lPartial2(source2, 1)(2);

    // assert
    expect(result).to.be.equal(5);
  });
});


