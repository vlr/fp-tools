import { expect } from "chai";
import { rPartial } from "../../src";

const source2 = (i1: number, i2: number) => i1 + i2 * 2;

describe("rPartial2", function (): void {
  it("should return partial1 function that sums 2 numbers in correct order", function (): void {
    // arrange

    // act
    const result = rPartial(source2, 2)(1);

    // assert
    expect(result).to.be.equal(5);
  });
});


