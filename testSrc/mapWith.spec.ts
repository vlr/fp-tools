import { expect } from "chai";
import { mapWith } from "../src";

describe("mapWith", function (): void {
  it("should map function with one argument", function (): void {
    // arrange
    const mappedFunc = (n: number) => n + 1;

    const testParams = [1, 2, 3];
    const expected = [2, 3, 4];

    // act
    const result = mapWith(mappedFunc)(testParams);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should map function with two argument", function (): void {
    // arrange
    const mappedFunc = (n: number, n1: number) => n + n1;

    const testParams = [1, 2, 3];
    const expected = [3, 4, 5];

    // act
    const result = mapWith(mappedFunc)(testParams, 2);

    // assert
    expect(result).deep.equals(expected);
  });

  it("should work with void function, just in case", function (): void {
    // arrange
    function mappedFunc(n: number, n1: number): void { return; }

    const testParams = [1, 2, 3];
    const expected = [undefined, undefined, undefined];

    // act
    const result = mapWith(mappedFunc)(testParams, 2);

    // assert
    expect(result).deep.equals(expected);
  });
});
