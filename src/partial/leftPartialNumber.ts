// 1 => 0
export function lPartial1<TR, TI>(target: (i: TI) => TR, i: TI): () => TR {
  return () => target(i);
}

// 2 => 0
export function lPartial2<TR, TI1, TI2>(target: (i1: TI1, i2: TI2) => TR, i1: TI1, i2: TI2): () => TR;
// 2 => 1
export function lPartial2<TR, TI1, TI2>(target: (i1: TI1, i2: TI2) => TR, i2: TI2): (i1: TI1) => TR;

export function lPartial2<TR>(target: (...args: any[]) => TR, ...args: any[]): (...args: any[]) => TR {
  return createPartial(target, 2, args);
}

// 3 => 0
export function lPartial3<TR, TI1, TI2, TI3>(target: (i1: TI1, i2: TI2, i3: TI3) => TR, i1: TI1, i2: TI2, i3: TI3): () => TR;
// 3 => 1
export function lPartial3<TR, TI1, TI2, TI3>(target: (i1: TI1, i2: TI2, i3: TI3) => TR, i2: TI2, i3: TI3): (i1: TI1) => TR;
// 3 => 2
export function lPartial3<TR, TI1, TI2, TI3>(target: (i1: TI1, i2: TI2, i3: TI3) => TR, i3: TI3): (i1: TI1, i2: TI2) => TR;
export function lPartial3<TR>(target: (...args: any[]) => TR, ...args: any[]): (...args: any[]) => TR {
  return createPartial(target, 3, args);
}

// 4 => 0
export function lPartial4<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i1: TI1, i2: TI2, i3: TI3, i4: TI4): () => TR;
// 4 => 1
export function lPartial4<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i2: TI2, i3: TI3, i4: TI4): (i1: TI1) => TR;
// 4 => 2
export function lPartial4<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i3: TI3, i4: TI4): (i1: TI1, i2: TI2) => TR;
// 4 => 3
export function lPartial4<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i4: TI4): (i1: TI1, i2: TI2, i3: TI3) => TR;

export function lPartial4<TR>(target: (...args: any[]) => TR, ...args: any[]): (...args: any[]) => TR {
  return createPartial(target, 4, args);
}

function createPartial<TR>(target: (...args: any[]) => TR, funcArgsCount: number, lastArgs: any[]): (...args: any[]) => TR {
  switch (funcArgsCount - lastArgs.length) {
    case 0:
      return () => target(...lastArgs);
    case 1:
      return i1 => target(...lastArgs, i1);
    case 2:
      return (i1, i2) => target(...lastArgs, i1, i2);
    case 3:
      return (i1, i2, i3) => target(...lastArgs, i1, i2, i3);
    default: throw new Error("Unexpected number of parameters");
  }
}
