export { lPartial as partial } from "./leftPartial";
export {
  lPartial1 as partial1,
  lPartial2 as partial2,
  lPartial3 as partial3,
  lPartial4 as partial4
} from "./leftPartialNumber";

export * from "./rightPartial";
export * from "./rightPartialNumber";
export * from "./leftPartial";
export * from "./leftPartialNumber";
