// 2 => 1
export function lPartial<TR, TI1, TI2>(target: (i1: TI1, i2: TI2) => TR, i1: TI1): (i2: TI2) => TR;
// 3 => 1
export function lPartial<TR, TI1, TI2, TI3>(target: (i1: TI1, i2: TI2, i3: TI3) => TR, i1: TI1, i2: TI2): (i3: TI3) => TR;
// 3 => 2
export function lPartial<TR, TI1, TI2, TI3>(target: (i1: TI1, i2: TI2, i3: TI3) => TR, i1: TI1): (i2: TI2, i3: TI3) => TR;
// 4 => 1
export function lPartial<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i1: TI1, i2: TI2, i3: TI3): (i4: TI4) => TR;
// 4 => 2
export function lPartial<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i1: TI1, i2: TI2): (i3: TI3, i4: TI4) => TR;
// 4 => 3
export function lPartial<TR, TI1, TI2, TI3, TI4>(target: (i1: TI1, i2: TI2, i3: TI3, i4: TI4) => TR, i1: TI1): (i2: TI2, i3: TI3, i4: TI4) => TR;

export function lPartial<TR>(target: (...args: any[]) => TR, ...firstArgs: any[]): (...args: any[]) => TR {
  return function (...lastArgs: any[]): TR {
    return target(...firstArgs, ...lastArgs);
  };
}
